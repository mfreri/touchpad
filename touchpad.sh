#! /bin/bash


: '
USAGE: touchpad [{"on" | "off" | options}]

Set touchpad on or off. If no argument is passed, display the device info.

More info: touchpad --help
'


###############
## CONSTANTS ##
###############

_VERSION="0.4"
_RELEASE="2021-11-22"
_CONTACT="mfreri@protonmail.com"


###############
## FUNCTIONS ##
###############

set_state(){
	# Set the touchpad state to $1
	if [ $1 = "on" ]; then
		# Touchpad is set to ON
		xinput --enable $device_id
	elif [ $1 = "off" ]; then
		# Touchpad is set to OFF
		xinput --disable $device_id
	fi
}


display_error(){
	# The error catcher
	if [ $1 = 1 ]; then
		echo "(E) Cannot detect touchpad's id. Does this computer have a touchpad?"
	else
		echo "(E) Unknown error ($1)."
	fi
	exit 1
}


##########
## MAIN ##
##########

# Detect touchpad id
_error=0
device_id=`xinput list | grep -i "touchpad" | awk '{print substr($6,4)}'`
# If none touchpad is detected
if [ -z "$device_id" ]; then
	_error=1
fi

# Detect touchpad state (0=off, 1=on)
if [ $_error -eq 0 ]; then
	device_current_state=`xinput list-props $device_id | grep "Device Enabled" | awk '{print $4}'`
fi

if [ $1 ]; then
	if [ $1 = "-h" ] || [ $1 = "--help" ]; then
		echo "touchpad - version $_VERSION"
		echo ""
		echo '    USAGE: touchpad [{"on" | "off" | options}]'
		echo ""
		echo "Description:"
		echo "    Set the touchpad ON or OFF. If none argument is passed, show"
		echo "    information about the device and the current state."
		echo ""
		echo "Options:"
		echo "    -c, --cycle        Change the state of the touchpad to the current opposite."
		echo "    -h, --help         Display this help."
		echo "    -v, --version      Show the program version."
		echo ""
		echo "A script by Marcelo Freri <$_CONTACT>."
		exit 0
	elif [ $1 = "-v" ] || [ $1 = "--version" ]; then
		echo "touchpad - version $_VERSION ($_RELEASE)"
		echo "A script by Marcelo Freri <$_CONTACT>"
		exit 0
	elif [ $1 = "on" ] || [ $1 = "off" ]; then
		if [ $_error -ne 0 ]; then
			display_error $_error
		else
			set_state $1
		fi
	elif [ $1 = "-c" ] || [ $1 = "--cycle" ]; then
		if [ $_error -ne 0 ]; then
			display_error $_error
		else
			# Cycle the touchpad state from on to off and viceversa
			if [ $device_current_state = 1 ]; then
				set_state "off"
				notify-send "Touchpad disabled."
			else
				set_state "on"
				notify-send "Touchpad enabled."
			fi
		fi
	else
		echo "(E) Invalid parameter '$1'."
		exit 1
	fi
else
	if [ $_error -ne 0 ]; then
		display_error $_error
	else
		xinput list | grep -i "touchpad"
		echo -n "The current state is: "
		if [ $device_current_state == 0 ]; then
			echo "OFF"
		else
			echo "ON"
		fi
	fi
fi
