# Touchpad
---
Touchpad is a Bash script to control the touchpad state in laptops.

## Usage
---
### Command line options
```
on
	Enables the touchpad
off
	Disables the touchpad
--cycle, -c
	Alternate the state between ENABLED and DISABLED
--help, -h
	Show help
--version, -v
	Display the script's version
```

### Default output
If no argument were passed, the script will show information about the Touchpad, such as brand and ID.

### Usage examples
Enable the touchpad:
```
touchpad on
```

Disable the touchpad:
```
touchpad off
```

Alternate the current state:
```
touchpad --cycle
```
